class SceneController extends THREE.Object3D {

    constructor(lib) {
        super();

        this.lib = lib;
        this.lib.mesh = {};
        this.lib.animatedMesh = {};
        this.state;
        this.steps = {};

        this.stepsHelper = document.querySelector(".stepsHelper");
        // console.log(this.stepsHelper.innerText);

        this.clock = new THREE.Clock();

        this.steps.init = new InitStep(this.lib);
        this.add(this.steps.init);

        this.steps.first = new FirstStep(this.lib);
        this.steps.second = new SecondStep(this.lib);
        this.loadStep("first");
        // this.loadStep("second");

        // setTimeout(() => {
        //     this.unloadStep("first");
        //     this.loadStep("second");
        // }, 2000);

        this.createLight();
    }

    update(time) {
        if (this.state == "first") {
            this.steps.first.update(time);
        } else if (this.state == "second") {
            this.steps.second.update(time);
        }
    }

    loadStep(stringName) {
        if (stringName == "first") {
            this.stepsHelper.innerText = "1";
            this.add(this.steps.first);
            this.state = "first";
        } else if (stringName == "second") {
            this.add(this.steps.second);
            this.state = "second";
        }
    }

    unloadStep(stringName) {
        if (stringName == "first") {
            this.remove(this.steps.first);
            this.state = "";
        }
    }

    createCoverBook() {

        const geometryCover = new THREE.BoxGeometry( 50, 50, 1 );
        const materialCover = new THREE.MeshPhongMaterial( {color: 0x379fcc} );
        const coverFront = new THREE.Mesh( geometryCover, materialCover );
        coverFront.rotation.set(Tools.degreesToRadians(90), 0, 0);
        this.add( coverFront );
        console.log(coverFront);
        coverFront.receiveShadow = true;
        coverFront.castShadow = true;


        const coverBack = new THREE.Mesh( geometryCover, materialCover );
        coverBack.position.set(0, 0, 5);
        // this.add(coverBack);

        const geometrySlice = new THREE.BoxGeometry( 50, 50, 1 );
        const materialSlice = new THREE.MeshPhongMaterial( {color: 0x379fcc} );
        const bookSlice = new THREE.Mesh(geometrySlice, materialSlice);
        bookSlice.position.set(50, 0, 0);
        // this.add(bookSlice);

        const geometryPlane = new THREE.PlaneGeometry( 100, 100, 32 );
        const materialPlane = new THREE.MeshPhongMaterial( {color: 0xFFFFFF, side: THREE.DoubleSide} );
        const plane = new THREE.Mesh( geometryPlane, materialPlane );

        plane.receiveShadow = true;
        plane.castShadow = true;
        plane.position.set(0, -2, 0);

    }

    createLight() {

        // const light = new THREE.AmbientLight( 0xFFFFFF ); // soft white light
        // this.add( light );

        // const ambient = new THREE.HemisphereLight(0xFFFFFF,0x000000,.7);
        // this.add(ambient);
        this.dirLight = new THREE.DirectionalLight(0xFFFFEE,2);

		this.dirLight.position.set( 0, 10, 0 );
		this.dirLight.castShadow = true;
        let shadowMapValue = 50;
		this.dirLight.shadow.camera.right = shadowMapValue;
		this.dirLight.shadow.camera.left = - shadowMapValue;
		this.dirLight.shadow.camera.top	= shadowMapValue;
		this.dirLight.shadow.camera.bottom = - shadowMapValue;
		this.dirLight.shadow.mapSize.width = 1024;
		this.dirLight.shadow.mapSize.height = 1024;

        this.dirLight.position.set(0,10,50);
        this.dirLight.target.position.set(0, 0, 0);
        this.add(this.dirLight);
        const helper = new THREE.DirectionalLightHelper( this.dirLight, 5 );
        // this.add( helper );

    }

}
