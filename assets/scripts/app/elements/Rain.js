class Rain extends THREE.Object3D {

    constructor() {
        super();
        this.createParticles();
    }

    createParticles() {

        var floatingGeo = new THREE.BufferGeometry();
        var floatingVertices = new Float32Array(400*3);
        var floatingDirection = new Float32Array(400*3);
        // floatingGeo.rateOfChange = [];
        // for(let i = 0; i < floatingVertices.length; i++){
        //     floatingGeo.rateOfChange.push(Math.floor(500+Math.random()*1000));
        // }
        for(let i = 0; i < floatingVertices.length; i = i+3 ){
            floatingDirection[i]   = Tools.getRange(-0.5, 0.5);
            floatingDirection[i+1] = -1;
            floatingDirection[i+2] = 0;
            // floatingDirection[i]   = Math.random()-.5;
            // floatingDirection[i+1] = Math.random()-.5;
            // floatingDirection[i+2] = Math.random()-.5;
            floatingVertices[i]    = (Math.random() - .5) * 2 * 80;
            floatingVertices[i+1]  = (Math.random() - .5) * 2 * 200;
            floatingVertices[i+2]  = (Math.random() - .5) * 2 * 90;
        }
        floatingGeo.addAttribute( 'position', new THREE.BufferAttribute( floatingVertices, 3 ) );
        floatingGeo.addAttribute( 'direction', new THREE.BufferAttribute( floatingDirection, 3 ) );

        var floatingMat = new THREE.ShaderMaterial( {
            uniforms:       {
                                time: {type: 'f',value: 0.0},
                                opacity: {type: 'f',value: 0.5}
                            },
            vertexShader:   document.getElementById( 'vFloating' ).textContent,
            fragmentShader: document.getElementById( 'fFloating' ).textContent,
            // blending:       THREE.AdditiveBlending,
            // depthTest:      false,
            side: THREE.DoubleSide,
            // fog: true,
            transparent:    true
        });

        this.floating = new THREE.Points(floatingGeo,floatingMat);
        this.add(this.floating);

    }

    update() {
        this.floating.material.uniforms.time.value += 0.1;
        // console.log(this.floating.material.uniforms.time.value);
        this.floating.material.uniforms.time.needsUpdate = true;
    }

}
