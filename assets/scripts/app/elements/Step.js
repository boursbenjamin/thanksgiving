class Step extends THREE.Object3D {

    constructor(lib) {
        super();
        this.lib = lib;

        this.animations = {
            mixers : {},
            actions : {}
        };
    }

    createMesh(elementName, scale={x,y,z}, clip) {

        if (this.lib.mesh[elementName]) {
            return this.lib.mesh[elementName].clone();
        } else {

        let geometry;
        let material;

        geometry = this.lib.geos[elementName];
        let matrix = new THREE.Matrix4();
        matrix.makeScale(scale.x,scale.y,scale.z);
        geometry.applyMatrix(matrix);
        geometry.computeBoundingBox();

        if (this.lib.mats[elementName]) {
            material = this.lib.mats[elementName];
        } else {
            material = new THREE.MeshPhongMaterial({color : 0xFFFFFF});
        }

        const mesh = new THREE.Mesh(geometry, material);

        if (clip) {
            let bookBBox = this.getMesh("livre.js").geometry.boundingBox;
            material.clippingPlanes = [];
            material.clipShadows = true;
            this.createClippingMask(material, new THREE.Vector3(1, 0, 0), new THREE.Vector3(bookBBox.min.x + 2, 0, 0));
            this.createClippingMask(material, new THREE.Vector3(-1, 0, 0), new THREE.Vector3(bookBBox.max.x - 2, 0, 0));
            this.createClippingMask(material, new THREE.Vector3(0, 1, 0), new THREE.Vector3(0, 0, 0));
        }

        mesh.geometry.faceVertexUvs = [[]];
        mesh.name = elementName;
        this.lib.mesh[elementName] = mesh;

        return mesh;

        }
    }

    createAnimatedMesh(elementName) {

        let geometry,
            material;

        if (this.lib.geos[elementName]) {
            geometry = this.lib.geos[elementName];
        } else {
            console.error("geometry not exist");
        }

        if (this.lib.mats[elementName]) {
            material = this.lib.mats[elementName];
            material.skinning = true;
        } else {
            material = new THREE.MeshLambertMaterial({color:0xFFFFFF});
            material.skinning = true;
        }

        let animatedMesh = new THREE.SkinnedMesh(geometry,material);
        animatedMesh.scale.set(0.4,0.4,0.4);

        const mixer = new THREE.AnimationMixer(animatedMesh);
        this.animations.mixers[elementName] = mixer;

        mixer.addEventListener( 'loop', function(e) {
            // console.log("anim finished", e);
        });

        // array of animationsClip in this geometry
        const clips = animatedMesh.geometry.animations;

        // the way to find an animation in an array of animation, by it name
        for (let i = 0; i < clips.length; i++) {
            let clip = THREE.AnimationClip.findByName(clips, clips[i].name);
            this.animations.actions[elementName+"-"+clips[i].name] = mixer.clipAction(clip);
        }

        this.lib.animatedMesh[elementName] = animatedMesh;
    }

    createTextMesh(elementName) {

        if (this.lib.mesh[elementName]) {
            return this.lib.mesh[elementName].clone();
        } else {

            let geometry;
            let material;

            // geometry = this.lib.geos[elementName];

            geometry = new THREE.TextGeometry( 'Turn me !', {
                font: this.lib.fonts[elementName],
                size: 5,
                height: 1,
                curveSegments: 12,
                bevelEnabled: false,
                bevelThickness: 10,
                bevelSize: 8,
                bevelSegments: 5
            });
            // let matrix = new THREE.Matrix4();
            // matrix.makeScale(scale.x,scale.y,scale.z);
            // geometry.applyMatrix(matrix);
            geometry.computeBoundingBox();

            if (this.lib.mats[elementName]) {
                material = this.lib.mats[elementName];
            } else {
                material = this.lib.mats["standard"];
            }

            const mesh = new THREE.Mesh(geometry, material);

            mesh.name = elementName;
            this.lib.mesh[elementName] = mesh;

            return mesh;

        }

    }

    getMesh(elementName) {
        return this.lib.mesh[elementName];
    }

    getAnimatedMesh(elementName) {
        return this.lib.animatedMesh[elementName];
    }

    createClippingMask(material, normal, translate) {

        const mask = new THREE.Plane(normal).translate(translate);
        material.clippingPlanes.push(mask);

    }

    updateAnimationMixer(time) {
        for (let i in this.animations.mixers) {
            this.animations.mixers[i].update(time);
        }
    }

}
